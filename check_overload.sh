#!/bin/sh

# Call overload stats script for every run in a sim

echo "Inbound: "
for i in 1 2 3
do
 echo "Run $i:"
 ./overload_stats.py $1/tornet/$i/bad-stuff.log.txt "Inbound"
done

echo
echo "==============================="
echo

echo "Outbound: "
for i in 1 2 3
do
 echo "Run $i:"
 ./overload_stats.py $1/tornet/$i/bad-stuff.log.txt "Outbound"
done
