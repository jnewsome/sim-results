#!/bin/sh

HK=""
DE=""

for i in $@
do
 HK="$HK $i/tornet-hk"
 DE="$DE $i/tornet-de"
done

tornettools plot $HK
mv tornet.plot.pages.pdf tornet-hk.plot.pages.pdf
tornettools plot $DE
mv tornet.plot.pages.pdf tornet-de.plot.pages.pdf
