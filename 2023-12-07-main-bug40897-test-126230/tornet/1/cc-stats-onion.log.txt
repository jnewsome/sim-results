
Network statistics for circuits >= 4MB xmit:
  Stats for phase post-4mb:
    BWE: Min: 1.0/65.7+174.8/2441.0, Avg: 1.0/177.1+246.1/2607.8, Dev: 81.9, Max: 1.0/335.7+342.9/2802.0
    CWND: Min: 238.0/375.6+146.9/2049.0, Avg: 242.0/437.9+158.6/2049.0, Dev: 44.5, Max: 242.0/507.6+197.8/2049.0
    QUSE: Min: 1.0/155.3+129.8/433.0, Avg: 7.0/4480796605980032.5+62992797484254792.0/3074457345618258432.0, Dev: 27013651684463764.0, Max: 7.0/205000561493097728.0+1933796179181623552.0/18446744073709551616.0
    final CWND: 241.0/404.2+165.5/2049.0
    final QUSE: 1.0/265.2+109.0/433.0
    RTTFactor: Min: 1.0/13.6+30.8/325.4, Avg: 1.0/37.1+44.2/325.4, Dev: 21.1, Max: 1.0/93.9+82.5/411.2
    init minRTTFactor: 1.0/1.2+0.4/13.3
    phase minRTTFactor: 1.0/1.1+0.2/9.0
    Total circs: 11158
  Stats for phase slow start:
    BWE: Min: 1.0/176.0+219.6/2480.0, Avg: 1.3/260.1+206.3/2480.0, Dev: 85.0, Max: 2.0/357.8+250.5/2480.0
    CWND: Min: 249.0/424.3+190.1/2049.0, Avg: 250.3/498.3+169.2/2049.0, Dev: 73.4, Max: 251.0/581.6+193.4/2049.0
    QUSE: Min: 248.0/248.0+0.0/248.0, Avg: 248.0/248.0+0.0/248.0, Dev: 0.0, Max: 248.0/248.0+0.0/248.0
    final CWND: 249.0/488.8+201.6/2049.0
    final QUSE: 248.0/248.0+0.0/248.0
    RTTFactor: Min: 1.0/3.2+5.9/92.0, Avg: 1.0/10.5+15.2/131.0, Dev: 7.5, Max: 1.0/19.4+30.9/204.6
    init minRTTFactor: 1.0/1.2+0.4/13.3
    phase minRTTFactor: 1.0/1.0+0.2/13.3
    Total circs: 11158
  Stats for phase post-slow start:
    BWE: Min: 1.0/39.2+126.2/2441.0, Avg: 1.1/185.8+225.5/2602.6, Dev: 118.0, Max: 2.0/456.3+308.9/2802.0
    CWND: Min: 238.0/352.1+136.5/2049.0, Avg: 244.4/453.0+151.5/2049.0, Dev: 74.1, Max: 251.0/585.6+195.1/2049.0
    QUSE: Min: 1.0/95.7+96.9/248.0, Avg: 18.7/13395358109890106.0+84044225358233968.0/2305843009213693952.0, Dev: 92140643586705792.0, Max: 248.0/730727807902816128.0+3597997461183593984.0/18446744073709551616.0
    final CWND: 241.0/404.2+165.5/2049.0
    final QUSE: 1.0/265.2+109.0/433.0
    RTTFactor: Min: 1.0/2.6+5.0/80.0, Avg: 1.0/32.8+33.3/207.3, Dev: 28.1, Max: 1.0/118.7+93.5/437.4
    init minRTTFactor: 1.0/1.2+0.4/13.3
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 11158
  Stats for phase lifetime:
    BWE: Min: 1.0/39.2+126.2/2441.0, Avg: 1.1/187.2+223.8/2601.7, Dev: 121.0, Max: 2.0/456.3+308.9/2802.0
    CWND: Min: 238.0/352.1+136.5/2049.0, Avg: 244.7/453.7+151.2/2049.0, Dev: 75.6, Max: 251.0/585.6+195.1/2049.0
    QUSE: Min: 1.0/95.7+96.9/248.0, Avg: 20.9/13097041882754146.0+81995769895121040.0/2249602935818237952.0, Dev: 91188043377083392.0, Max: 248.0/730727807902816128.0+3597997461183593984.0/18446744073709551616.0
    final CWND: 241.0/404.2+165.5/2049.0
    final QUSE: 1.0/265.2+109.0/433.0
    RTTFactor: Min: 1.0/2.6+5.0/80.0, Avg: 1.0/32.2+32.5/202.3, Dev: 28.2, Max: 1.0/118.7+93.5/437.4
    init minRTTFactor: 1.0/1.2+0.4/13.3
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 11158
  Total circs: 11158
  Circs still in SS: 0

Network statistics for all circuits:
  Stats for phase post-4mb:
    BWE: Min: 1.0/65.7+174.8/2441.0, Avg: 1.0/177.1+246.1/2607.8, Dev: 81.9, Max: 1.0/335.7+342.9/2802.0
    CWND: Min: 238.0/375.6+146.9/2049.0, Avg: 242.0/437.9+158.6/2049.0, Dev: 44.5, Max: 242.0/507.6+197.8/2049.0
    QUSE: Min: 1.0/155.3+129.8/433.0, Avg: 7.0/4480796605980032.5+62992797484254792.0/3074457345618258432.0, Dev: 27013651684463764.0, Max: 7.0/205000561493097728.0+1933796179181623552.0/18446744073709551616.0
    final CWND: 241.0/404.2+165.5/2049.0
    final QUSE: 1.0/265.2+109.0/433.0
    RTTFactor: Min: 1.0/13.6+30.8/325.4, Avg: 1.0/37.1+44.2/325.4, Dev: 21.1, Max: 1.0/93.9+82.5/411.2
    init minRTTFactor: 1.0/1.2+0.4/13.3
    phase minRTTFactor: 1.0/1.1+0.2/9.0
    Total circs: 11158
  Stats for phase slow start:
    BWE: Min: 1.0/134.1+198.5/2480.0, Avg: 1.0/180.7+203.2/2480.0, Dev: 47.1, Max: 1.0/234.1+248.9/2480.0
    CWND: Min: 249.0/383.3+174.6/2049.0, Avg: 249.0/424.5+174.6/2049.0, Dev: 41.0, Max: 249.0/470.6+207.0/2049.0
    QUSE: Min: 248.0/248.0+0.0/248.0, Avg: 248.0/248.0+0.0/248.0, Dev: 0.0, Max: 248.0/248.0+0.0/248.0
    final CWND: 249.0/419.0+190.7/2049.0
    final QUSE: 248.0/248.0+0.0/248.0
    RTTFactor: Min: 1.0/16.6+32.5/254.1, Avg: 1.0/22.4+34.1/254.1, Dev: 5.9, Max: 1.0/29.1+41.3/254.1
    init minRTTFactor: 1.0/1.1+0.3/13.3
    phase minRTTFactor: 1.0/1.0+0.1/13.3
    Total circs: 23644
  Stats for phase post-slow start:
    BWE: Min: 1.0/37.1+119.2/2441.0, Avg: 1.0/126.5+200.0/2602.6, Dev: 74.3, Max: 1.0/294.3+308.9/2802.0
    CWND: Min: 236.0/336.5+130.6/2049.0, Avg: 242.6/395.1+153.5/2049.0, Dev: 44.3, Max: 249.0/472.6+208.8/2049.0
    QUSE: Min: 1.0/155.0+102.5/248.0, Avg: 18.7/11763099249484094.0+112917723282265936.0/3689348814741910528.0, Dev: 62640184387322864.0, Max: 248.0/425201975984254144.0+2768175447878298112.0/18446744073709551616.0
    final CWND: 236.0/365.6+152.0/2049.0
    final QUSE: 1.0/266.9+93.3/433.0
    RTTFactor: Min: 1.0/14.4+29.9/254.1, Avg: 1.0/50.0+51.1/337.6, Dev: 27.8, Max: 1.0/117.2+92.8/468.3
    init minRTTFactor: 1.0/1.1+0.3/13.3
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 23644
  Stats for phase lifetime:
    BWE: Min: 1.0/37.1+119.2/2441.0, Avg: 1.0/128.2+198.6/2601.7, Dev: 77.0, Max: 1.0/294.3+308.9/2802.0
    CWND: Min: 236.0/336.5+130.6/2049.0, Avg: 243.0/395.8+153.6/2049.0, Dev: 45.4, Max: 249.0/472.6+208.8/2049.0
    QUSE: Min: 1.0/155.0+102.5/248.0, Avg: 20.9/11328637093087456.0+107337225598503232.0/3458764513820540928.0, Dev: 61741986266013392.0, Max: 248.0/425201975984254144.0+2768175447878298112.0/18446744073709551616.0
    final CWND: 236.0/365.6+152.0/2049.0
    final QUSE: 1.0/266.9+93.3/433.0
    RTTFactor: Min: 1.0/14.4+29.9/254.1, Avg: 1.0/48.4+49.2/322.0, Dev: 28.1, Max: 1.0/117.2+92.8/468.3
    init minRTTFactor: 1.0/1.1+0.3/13.3
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 23644
  Total circs: 23644
  Circs still in SS: 0

Network statistics for circuits never exiting slow start:
  No circuits

Network statistics for circuits that hit min cwnd:
  No circuits
