
Network statistics for circuits >= 4MB xmit:
  Stats for phase post-4mb:
    BWE: Min: 1.0/1084.5+612.8/5692.0, Avg: 20.6/1555.0+648.2/6869.4, Dev: 189.9, Max: 31.0/1875.7+742.6/8019.0
    CWND: Min: 187.0/633.9+187.5/2128.0, Avg: 234.2/665.5+177.2/2141.8, Dev: 19.8, Max: 235.0/695.6+176.7/2261.0
    QUSE: Min: 1.0/17.7+33.5/309.0, Avg: 1.5/43274448896670.5+4170276574843533.0/945986875574848768.0, Dev: 369036218979694.6, Max: 2.0/3947049606131721.0+269804808574559488.0/18446744073709551616.0
    final CWND: 190.0/652.6+180.2/2150.0
    final QUSE: 0.0/102.8+78.2/309.0
    RTTFactor: Min: 1.0/1.0+0.1/25.7, Avg: 1.0/1.3+0.4/57.5, Dev: 0.3, Max: 1.0/2.3+4.1/507.7
    init minRTTFactor: 1.0/1.0+0.1/10.8
    phase minRTTFactor: 1.0/1.0+0.0/10.7
    Total circs: 177597
  Stats for phase slow start:
    BWE: Min: 1.0/1337.9+599.2/6452.0, Avg: 3.0/1343.7+593.8/6452.0, Dev: 4.4, Max: 3.0/1351.6+600.8/6452.0
    CWND: Min: 187.0/689.0+183.3/2125.0, Avg: 191.0/690.9+181.0/2125.0, Dev: 1.3, Max: 191.0/693.0+182.2/2256.0
    QUSE: Min: 186.0/186.0+0.0/186.0, Avg: 186.0/186.0+0.0/186.0, Dev: 0.0, Max: 186.0/186.0+0.0/186.0
    final CWND: 190.0/691.0+181.7/2125.0
    final QUSE: 186.0/186.0+0.0/186.0
    RTTFactor: Min: 1.0/1.5+0.3/42.1, Avg: 1.0/1.5+0.3/42.1, Dev: 0.0, Max: 1.0/1.5+1.3/206.8
    init minRTTFactor: 1.0/1.0+0.1/9.2
    phase minRTTFactor: 1.0/1.0+0.1/7.5
    Total circs: 177597
  Stats for phase post-slow start:
    BWE: Min: 1.0/978.2+565.1/5535.0, Avg: 96.3/1552.8+641.0/6867.9, Dev: 220.1, Max: 169.0/1927.4+751.4/8583.0
    CWND: Min: 187.0/630.8+189.0/2125.0, Avg: 214.6/669.4+175.9/2140.4, Dev: 25.2, Max: 235.0/709.2+174.8/2261.0
    QUSE: Min: 1.0/10.0+16.9/186.0, Avg: 10.7/149094239158456.4+10180647731366786.0/2213609288845146112.0, Dev: 1079183772136288.6, Max: 186.0/10386855675326470.0+437602309868772224.0/18446744073709551616.0
    final CWND: 190.0/652.6+180.2/2150.0
    final QUSE: 1.0/102.8+78.2/309.0
    RTTFactor: Min: 1.0/1.0+0.0/2.6, Avg: 1.0/1.3+0.3/40.2, Dev: 0.3, Max: 1.0/2.5+4.2/507.7
    init minRTTFactor: 1.0/1.0+0.1/9.2
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 177597
  Stats for phase lifetime:
    BWE: Min: 1.0/978.2+565.1/5535.0, Avg: 95.7/1551.1+640.3/6866.5, Dev: 221.3, Max: 169.0/1927.4+751.4/8583.0
    CWND: Min: 187.0/630.8+189.0/2125.0, Avg: 214.2/669.5+175.8/2140.0, Dev: 25.4, Max: 235.0/709.2+174.8/2261.0
    QUSE: Min: 1.0/10.0+16.9/186.0, Avg: 11.1/146679373957037.8+9943034590045766.0/2128470470043409920.0, Dev: 1072386962776103.9, Max: 186.0/10386855675326470.0+437602309868772224.0/18446744073709551616.0
    final CWND: 190.0/652.6+180.2/2150.0
    final QUSE: 1.0/102.8+78.2/309.0
    RTTFactor: Min: 1.0/1.0+0.0/2.6, Avg: 1.0/1.3+0.3/39.8, Dev: 0.3, Max: 1.0/2.5+4.2/507.7
    init minRTTFactor: 1.0/1.0+0.1/9.2
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 177597
  Total circs: 177597
  Circs still in SS: 0

Network statistics for all circuits:
  Stats for phase post-4mb:
    BWE: Min: 1.0/1084.5+612.8/5692.0, Avg: 20.6/1555.0+648.2/6869.4, Dev: 189.9, Max: 31.0/1875.7+742.6/8019.0
    CWND: Min: 187.0/633.9+187.5/2128.0, Avg: 234.2/665.5+177.2/2141.8, Dev: 19.8, Max: 235.0/695.6+176.7/2261.0
    QUSE: Min: 1.0/17.7+33.5/309.0, Avg: 1.5/43274448896670.5+4170276574843533.0/945986875574848768.0, Dev: 369036218979694.6, Max: 2.0/3947049606131721.0+269804808574559488.0/18446744073709551616.0
    final CWND: 190.0/652.6+180.2/2150.0
    final QUSE: 0.0/102.8+78.2/309.0
    RTTFactor: Min: 1.0/1.0+0.1/25.7, Avg: 1.0/1.3+0.4/57.5, Dev: 0.3, Max: 1.0/2.3+4.1/507.7
    init minRTTFactor: 1.0/1.0+0.1/10.8
    phase minRTTFactor: 1.0/1.0+0.0/10.7
    Total circs: 177597
  Stats for phase slow start:
    BWE: Min: 1.0/1313.0+593.6/6562.0, Avg: 3.0/1317.3+589.8/6562.0, Dev: 3.3, Max: 3.0/1323.1+595.3/6562.0
    CWND: Min: 187.0/675.2+180.8/2125.0, Avg: 190.0/676.6+179.1/2125.0, Dev: 0.9, Max: 190.0/678.1+180.1/2256.0
    QUSE: Min: 186.0/186.0+0.0/186.0, Avg: 186.0/186.0+0.0/186.0, Dev: 0.0, Max: 186.0/186.0+0.0/186.0
    final CWND: 190.0/676.6+179.7/2125.0
    final QUSE: 186.0/186.0+0.0/186.0
    RTTFactor: Min: 1.0/1.5+0.4/84.0, Avg: 1.0/1.5+0.4/84.0, Dev: 0.0, Max: 1.0/1.5+1.1/206.8
    init minRTTFactor: 1.0/1.0+0.1/9.2
    phase minRTTFactor: 1.0/1.0+0.1/7.5
    Total circs: 256192
  Stats for phase post-slow start:
    BWE: Min: 1.0/1019.1+565.0/6372.0, Avg: 11.5/1502.7+644.8/7356.0, Dev: 200.0, Max: 15.0/1832.7+760.0/8583.0
    CWND: Min: 183.0/631.7+183.6/2125.0, Avg: 187.2/660.3+174.4/2140.4, Dev: 18.8, Max: 191.0/689.6+175.8/2261.0
    QUSE: Min: 1.0/30.7+52.5/186.0, Avg: 10.7/126501180835225.3+11169698536234710.0/3458764513820540928.0, Dev: 827737857549039.9, Max: 186.0/7560377091164060.0+373372711910872640.0/18446744073709551616.0
    final CWND: 190.0/647.1+177.3/2150.0
    final QUSE: 1.0/112.1+82.2/309.0
    RTTFactor: Min: 1.0/1.1+0.2/22.3, Avg: 1.0/1.3+0.5/133.6, Dev: 0.3, Max: 1.0/2.3+3.6/507.7
    init minRTTFactor: 1.0/1.0+0.1/9.2
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 256192
  Stats for phase lifetime:
    BWE: Min: 1.0/1019.1+565.0/6372.0, Avg: 11.9/1499.9+643.2/7267.8, Dev: 201.7, Max: 15.0/1832.7+760.0/8583.0
    CWND: Min: 183.0/631.7+183.6/2125.0, Avg: 187.4/660.5+174.3/2140.0, Dev: 18.9, Max: 191.0/689.6+175.8/2261.0
    QUSE: Min: 1.0/30.7+52.5/186.0, Avg: 11.1/123774352818056.9+10760701366055630.0/3255307777713450496.0, Dev: 821694604244863.5, Max: 186.0/7560377091164060.0+373372711910872640.0/18446744073709551616.0
    final CWND: 190.0/647.1+177.3/2150.0
    final QUSE: 1.0/112.1+82.2/309.0
    RTTFactor: Min: 1.0/1.1+0.2/22.3, Avg: 1.0/1.3+0.5/121.6, Dev: 0.3, Max: 1.0/2.3+3.6/507.7
    init minRTTFactor: 1.0/1.0+0.1/9.2
    phase minRTTFactor: 1.0/1.0+0.0/1.0
    Total circs: 256192
  Total circs: 256192
  Circs still in SS: 0

Network statistics for circuits never exiting slow start:
  No circuits

Network statistics for circuits that hit min cwnd:
  No circuits
TLS overhead in /shadow/bug40842-test/99141/350904/tornet/3/heartbeat-nonexit.log min/avg+dev/max: 0.68/2.92+6.7/66.78
TLS overhead in /shadow/bug40842-test/99141/350904/tornet/3/heartbeat-exit.log min/avg+dev/max: 0.68/2.15+2.1/50.18
