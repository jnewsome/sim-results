#!/usr/bin/env python3
import stem
import stem.control
import sys
import os

sorted_r = []
nonexit_r = []
fast_r = []
guard_r = []

def sort_routers(routers):
  sorted_r = list(routers)

  sorted_r.sort(key = lambda x: x.bandwidth, reverse = True)

  return sorted_r

def write_exclude_lists(guard_pct, fast_pct, c_valid, header=True):
  guard_idx = int(guard_pct*len(sorted_r))
  guard_idx_ne = int(guard_pct*len(nonexit_r))
  fast_idx = int(fast_pct*len(sorted_r))

  guard_bw = min(sorted_r[guard_idx].bandwidth,
                 nonexit_r[guard_idx_ne].bandwidth)
  fast_bw = sorted_r[fast_idx].bandwidth

  too_slow_g = 0
  tot_guard = 0
  cut_guard = 0
  G = 0
  M = 0
  E = 0
  D = 0
  T = 0
  guard_excl = []
  for r in guard_r:
    tot_guard += r.bandwidth
    if r.bandwidth < guard_bw:
      too_slow_g += 1
      cut_guard += r.bandwidth
      guard_excl.append(r.fingerprint)
    else:
      if not "Exit" in r.flags:
        G += r.bandwidth

  too_slow_f = 0
  tot_fast = 0
  cut_fast = 0
  fast_excl = []
  for r in fast_r:
    tot_fast += r.bandwidth
    if r.bandwidth < fast_bw:
      too_slow_f += 1
      cut_fast += r.bandwidth
      fast_excl.append(r.fingerprint)
    else:
      T += r.bandwidth
      if "Exit" in r.flags and "Guard" in r.flags:
        D += r.bandwidth
      elif "Exit" in r.flags:
        E += r.bandwidth
      elif not "Guard" in r.flags:
        M += r.bandwidth

  # XXX: Compute Wxx weights?

  outfile_name = "g%d-f%d" % (int(100*guard_pct), int(100*fast_pct))
  c_date = c_valid.split()[0]

  fast_f = open(c_date+"-"+outfile_name+"-fast_excl.txt", "w")
  guard_f = open(c_date+"-"+outfile_name+"-guard_excl.txt", "w")

  header_str = ""
  if header:
    header_str += "# From Consensus: "+c_valid+"\n"
    header_str += "# fast-speed="+str(sorted_r[fast_idx].bandwidth)+"\n"
    header_str += "# guard-bw-inc-exits="+str(sorted_r[guard_idx].bandwidth)+"\n"
    header_str += "# guard-bw-exc-exits="+str(nonexit_r[guard_idx_ne].bandwidth)+"\n"
    header_str += "# Cut Guard Weight: %.2f%%" % (100.0*cut_guard/tot_guard)+"\n"
    header_str += "# Cut Fast Weight: %.2f%%" % (100.0*cut_fast/tot_fast)+"\n"
    header_str += "# G=%.3f M=%.3f E=%.3f D=%.3f" % (100.0*G/T, 100.0*M/T, 100.0*E/T, 100.0*D/T)
    header_str += "\n\n"
    header_str += "# Slowest allowed Guard: "+ guard_r[-too_slow_g-1].fingerprint+"\n"
    header_str += "# Slowest allowed Fast: "+ fast_r[-too_slow_f-1].fingerprint+"\n"
    header_str += "\n\n"

    header_top_f = "# "+outfile_name+"-fast_excl (%.3f, %.3f) " % (guard_pct, fast_pct) +"\n"
    fast_f.write(header_top_f)
    fast_f.write(header_str)

    header_top_g = "# "+outfile_name+"-guard_excl (%.3f, %.3f) " % (guard_pct, fast_pct) +"\n"
    guard_f.write(header_top_g)
    guard_f.write(header_str)

  for r in fast_excl:
    fast_f.write(r+"\n")

  for r in guard_excl:
    guard_f.write(r+"\n")

def write_exit_excludes(c_valid, header=True):
  c_date = c_valid.split()[0]

  tot_exit = 0
  for r in sorted_r:
    if "Exit" in r.flags:
      tot_exit += r.bandwidth

  cut_exit_r = list(filter(lambda x: "Exit" in x.flags and not 2 in x.protocols["FlowCtrl"], sorted_r))
  cut_exit_r.sort(key = lambda x: x.bandwidth, reverse = True)

  cut_exit = 0
  for r in cut_exit_r:
    cut_exit += r.bandwidth

  exit_f = open(c_date+"-exit_excl.txt", "w")

  header_str = ""
  if header:
    header_str += "# From Consensus: "+c_valid+"\n"
    header_str += "# Cut Exit Weight: %.2f%%" % (100.0*cut_exit/tot_exit)+"\n"
    header_str += "# Kept Exit Weight: %.2f%%" % (100.0-100.0*cut_exit/tot_exit)+"\n"

    header_str += "\n\n"
    exit_f.write(header_str)

  for r in cut_exit_r:
    exit_f.write(r.fingerprint+"\n")

def parse_consensus(consensus_filename):
  doc = next(stem.descriptor.parse_file(consensus_filename,
                          document_handler =
                            stem.descriptor.DocumentHandler.DOCUMENT))

  assert(doc.is_consensus or doc.is_vote)
  return doc


# Parse consensus, micro-consensus, or vote if we have an argument, else use
# control port.
if len(sys.argv) > 1:
  consensus_file = sys.argv[1]
else:
  controller = stem.control.Controller.from_port("127.0.0.1")
  controller.authenticate()
  consensus_file = os.path.join(controller.get_conf("DataDirectory"),
                             "cached-microdesc-consensus")

doc = parse_consensus(consensus_file)
routers = doc.routers.values()
c_valid = str(doc.valid_after)

sorted_r = sort_routers(routers)
nonexit_r = list(filter(lambda x: "BadExit" in x.flags or not "Exit" in x.flags, sorted_r))
nonexit_r.sort(key = lambda x: x.bandwidth, reverse = True)

fast_r = list(filter(lambda x: "Fast" in x.flags, sorted_r))
fast_r.sort(key = lambda x: x.bandwidth, reverse = True)

guard_r = list(filter(lambda x: "Guard" in x.flags, sorted_r))
guard_r.sort(key = lambda x: x.bandwidth, reverse = True)

write_exit_excludes(c_valid)

write_exclude_lists(2.0/3.0, 3.0/4.0, c_valid)
write_exclude_lists(1.0/2.0, 2.0/3.0, c_valid)
write_exclude_lists(1.0/3.0, 2.0/3.0, c_valid)
write_exclude_lists(1.0/4.0, 3.0/4.0, c_valid)
write_exclude_lists(1.0/4.0, 2.0/3.0, c_valid)
write_exclude_lists(1.0/5.0, 1.0/2.0, c_valid)



