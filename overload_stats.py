#!/usr/bin/env python3

import sys
import re
import math

class Circ:
  def __init__(self):
    self.messages = 0
    self.lines = 0

class Relay:
  def __init__(self):
    self.messages = 0
    self.lines = 0

circs_all = {}
circs_500 = {}
circs_750 = {}
circs_900 = {}
circs_1k = {}
circs_125k = {}
circs_15k = {}
circs_175k = {}
circs_2k = {}
circs_225k = {}
circs_25k = {}
circs_275k = {}
circs_3k = {}
circs_4k = {}
circs_Nk = {}

relays_all = {}
relays_500 = {}
relays_750 = {}
relays_900 = {}
relays_1k = {}
relays_125k = {}
relays_15k = {}
relays_175k = {}
relays_2k = {}
relays_225k = {}
relays_25k = {}
relays_275k = {}
relays_3k = {}
relays_4k = {}
relays_Nk = {}

def add_circ(circs, relays, circ_id, relay_id, messages):
  if not circ_id in circs:
    circs[circ_id] = Circ()

  if not relay_id in relays:
    relays[relay_id] = Relay()

  circs[circ_id].lines += 1
  relays[relay_id].lines += 1

  circs[circ_id].messages += messages
  relays[relay_id].messages += messages

f = open(sys.argv[1])
for l in f:
  if not "cells in its queue" in l:
    continue

  if not " CIRC=" in l:
    continue

  if not sys.argv[2]+" circuit" in l:
    continue

  # "Inbound circuit relay222guard CIRC=48:4198515616 has 1741 cells in its queue,
  # maximum allowed is 1116. Closing circuit for safety reasons. [458 similar
  # message(s) suppressed in last 60 seconds]"

  circ_id_re = re.search(" ([^ ]+) CIRC=([^ ]+) ", l).groups()
  circ_id = circ_id_re[0]+circ_id_re[1]
  relay_id = circ_id_re[0]

  cells = int(re.search("([\d]+) cells in its queue", l).groups()[0])
  messages = 1

  if "suppressed in last" in l:
    messages = int(re.search("([\d]+) similar message", l).groups()[0])

  add_circ(circs_all, relays_all, circ_id, relay_id, messages)

  if cells >= 5000:
    add_circ(circs_Nk, relays_Nk, circ_id, relay_id, messages)
  elif cells >= 4000:
    add_circ(circs_4k, relays_4k, circ_id, relay_id, messages)
  elif cells >= 3000:
    add_circ(circs_3k, relays_3k, circ_id, relay_id, messages)
  elif cells >= 2750:
    add_circ(circs_275k, relays_275k, circ_id, relay_id, messages)
  elif cells >= 2500:
    add_circ(circs_25k, relays_25k, circ_id, relay_id, messages)
  elif cells >= 2250:
    add_circ(circs_225k, relays_225k, circ_id, relay_id, messages)
  elif cells >= 2000:
    add_circ(circs_2k, relays_2k, circ_id, relay_id, messages)
  elif cells >= 1750:
    add_circ(circs_175k, relays_175k, circ_id, relay_id, messages)
  elif cells >= 1500:
    add_circ(circs_15k, relays_15k, circ_id, relay_id, messages)
  elif cells >= 1250:
    add_circ(circs_125k, relays_125k, circ_id, relay_id, messages)
  elif cells >= 1000:
    add_circ(circs_1k, relays_1k, circ_id, relay_id, messages)
  elif cells >= 900:
    add_circ(circs_900, relays_900, circ_id, relay_id, messages)
  elif cells >= 750:
    add_circ(circs_750, relays_750, circ_id, relay_id, messages)
  elif cells >= 500:
    add_circ(circs_500, relays_500, circ_id, relay_id, messages)

def flag(relays, flag):
  return len(list(filter(lambda x: flag in x, relays.keys())))

print(" Circs, Relays:")
print("  500:   ", (len(circs_500), flag(relays_500, "guard"), flag(relays_1k, "middle"), flag(relays_1k, "exit")))
#if len(relays_500): print("    Relays: "+str(relays_500.keys()))
print("  750:   ", (len(circs_750), flag(relays_750, "guard"), flag(relays_1k, "middle"), flag(relays_1k, "exit")))
if len(relays_750): print("    Relays: "+str(relays_750.keys()))
print("  900:   ", (len(circs_900), flag(relays_900, "guard"), flag(relays_1k, "middle"), flag(relays_1k, "exit")))
if len(relays_900): print("    Relays: "+str(relays_900.keys()))
print("  1k:   ", (len(circs_1k), flag(relays_1k, "guard"), flag(relays_1k, "middle"), flag(relays_1k, "exit")))
if len(relays_1k): print("    Relays: "+str(relays_1k.keys()))
print("  1.25k:", (len(circs_125k), flag(relays_125k, "guard"), flag(relays_125k, "middle"), flag(relays_125k, "exit")))
if len(relays_125k): print("    Relays: "+str(relays_125k.keys()))
print("  1.5k: ", (len(circs_15k), flag(relays_15k, "guard"), flag(relays_15k, "middle"), flag(relays_15k, "exit")))
if len(relays_15k): print("    Relays: "+str(relays_15k.keys()))
print("  1.75k:", (len(circs_175k), flag(relays_175k, "guard"), flag(relays_175k, "middle"), flag(relays_175k, "exit")))
if len(relays_175k): print("    Relays: "+str(relays_175k.keys()))
print("  2k:   ", (len(circs_2k), flag(relays_2k, "guard"), flag(relays_2k, "middle"), flag(relays_2k, "exit")))
if len(relays_2k): print("    Relays: "+str(relays_2k.keys()))
print("  2.25k:", (len(circs_225k), flag(relays_225k, "guard"), flag(relays_225k, "middle"), flag(relays_225k, "exit")))
if len(relays_225k): print("    Relays: "+str(relays_225k.keys()))
print("  2.5k: ", (len(circs_25k), flag(relays_25k, "guard"), flag(relays_25k, "middle"), flag(relays_25k, "exit")))
if len(relays_25k): print("    Relays: "+str(relays_25k.keys()))
print("  2.75k:", (len(circs_275k), flag(relays_275k, "guard"), flag(relays_275k, "middle"), flag(relays_275k, "exit")))
if len(relays_275k): print("    Relays: "+str(relays_275k.keys()))
print("  3k:", (len(circs_3k), flag(relays_3k, "guard"), flag(relays_3k, "middle"), flag(relays_3k, "exit")))
if len(relays_3k): print("    Relays: "+str(relays_3k.keys()))
print("  4k:", (len(circs_4k), flag(relays_4k, "guard"), flag(relays_4k, "middle"), flag(relays_4k, "exit")))
if len(relays_4k): print("    Relays: "+str(relays_4k.keys()))
print("  Nk:", (len(circs_Nk), flag(relays_Nk, "guard"), flag(relays_Nk, "middle"), flag(relays_Nk, "exit")))
if len(relays_Nk): print("    Relays: "+str(relays_Nk.keys()))
print("")
